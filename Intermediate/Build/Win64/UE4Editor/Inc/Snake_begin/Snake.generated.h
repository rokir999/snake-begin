// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ASnakeElementBase;
class AActor;
#ifdef SNAKE_BEGIN_Snake_generated_h
#error "Snake.generated.h already included, missing '#pragma once' in Snake.h"
#endif
#define SNAKE_BEGIN_Snake_generated_h

#define Snake_begin_Source_Snake_begin_Snake_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSnakeElementOverlap) \
	{ \
		P_GET_OBJECT(ASnakeElementBase,Z_Param_OverlappedElement); \
		P_GET_OBJECT(AActor,Z_Param_Other); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SnakeElementOverlap(Z_Param_OverlappedElement,Z_Param_Other); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMove) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Move(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAddSnakeElement) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_ElementsNum); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AddSnakeElement(Z_Param_ElementsNum); \
		P_NATIVE_END; \
	}


#define Snake_begin_Source_Snake_begin_Snake_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSnakeElementOverlap) \
	{ \
		P_GET_OBJECT(ASnakeElementBase,Z_Param_OverlappedElement); \
		P_GET_OBJECT(AActor,Z_Param_Other); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SnakeElementOverlap(Z_Param_OverlappedElement,Z_Param_Other); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMove) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Move(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAddSnakeElement) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_ElementsNum); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AddSnakeElement(Z_Param_ElementsNum); \
		P_NATIVE_END; \
	}


#define Snake_begin_Source_Snake_begin_Snake_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnake(); \
	friend struct Z_Construct_UClass_ASnake_Statics; \
public: \
	DECLARE_CLASS(ASnake, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Snake_begin"), NO_API) \
	DECLARE_SERIALIZER(ASnake)


#define Snake_begin_Source_Snake_begin_Snake_h_23_INCLASS \
private: \
	static void StaticRegisterNativesASnake(); \
	friend struct Z_Construct_UClass_ASnake_Statics; \
public: \
	DECLARE_CLASS(ASnake, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Snake_begin"), NO_API) \
	DECLARE_SERIALIZER(ASnake)


#define Snake_begin_Source_Snake_begin_Snake_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnake(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnake) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnake); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnake); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnake(ASnake&&); \
	NO_API ASnake(const ASnake&); \
public:


#define Snake_begin_Source_Snake_begin_Snake_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnake(ASnake&&); \
	NO_API ASnake(const ASnake&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnake); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnake); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnake)


#define Snake_begin_Source_Snake_begin_Snake_h_23_PRIVATE_PROPERTY_OFFSET
#define Snake_begin_Source_Snake_begin_Snake_h_20_PROLOG
#define Snake_begin_Source_Snake_begin_Snake_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_begin_Source_Snake_begin_Snake_h_23_PRIVATE_PROPERTY_OFFSET \
	Snake_begin_Source_Snake_begin_Snake_h_23_RPC_WRAPPERS \
	Snake_begin_Source_Snake_begin_Snake_h_23_INCLASS \
	Snake_begin_Source_Snake_begin_Snake_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_begin_Source_Snake_begin_Snake_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_begin_Source_Snake_begin_Snake_h_23_PRIVATE_PROPERTY_OFFSET \
	Snake_begin_Source_Snake_begin_Snake_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_begin_Source_Snake_begin_Snake_h_23_INCLASS_NO_PURE_DECLS \
	Snake_begin_Source_Snake_begin_Snake_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_BEGIN_API UClass* StaticClass<class ASnake>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_begin_Source_Snake_begin_Snake_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EMovementDirection::UP) \
	op(EMovementDirection::DOWN) \
	op(EMovementDirection::LEFT) \
	op(EMovementDirection::RIGHT) 

enum class EMovementDirection;
template<> SNAKE_BEGIN_API UEnum* StaticEnum<EMovementDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
