// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Snake_beginGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_BEGIN_API ASnake_beginGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
